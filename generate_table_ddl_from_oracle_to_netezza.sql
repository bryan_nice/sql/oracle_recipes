WITH column_def as (
    SELECT
        a.TABLE_NAME,
        a.COLUMN_NAME || ' ' ||
        CASE
            WHEN a.DATA_TYPE = 'VARCHAR2'
            THEN 'VARCHAR(' || a.DATA_LENGTH || ')'
            WHEN a.DATA_TYPE = 'DATE'
                OR a.DATA_TYPE LIKE '%TIMESTAMP%'
            THEN 'TIMESTAMP'
            WHEN a.DATA_TYPE = 'FLOAT'
            THEN 'DOUBLE PRECISION'
            WHEN a.DATA_TYPE = 'NUMBER'
            THEN
                CASE
                    WHEN a.DATA_PRECISION IS NOT NULL
                        OR a.DATA_SCALE IS NOT NULL
                    THEN 'NUMERIC('||CAST(NVL(a.DATA_PRECISION,18) AS VARCHAR(22))||', '||CAST(NVL(a.DATA_SCALE,0) AS VARCHAR(22))||')'
                    ELSE 'BIGINT'
                END
            ELSE a.DATA_TYPE
        END col_def
    FROM
        ALL_TAB_COLS a
        JOIN
        (
            SELECT
                TABLE_NAME,
                MAX(COLUMN_ID) COLUMN_ID
            FROM
                ALL_TAB_COLS
            GROUP BY
                TABLE_NAME
        ) b ON
                a.TABLE_NAME = b.TABLE_NAME
/*
    WHERE
        a.TABLE_NAME in (<INPUT TABLE LIST HERE>)
*/
    ORDER BY
        a.TABLE_NAME,
        a.COLUMN_ID
)
SELECT
    TABLE_NAME,
    'CREATE TABLE '||TABLE_NAME||' (
    '||LISTAGG(col_def, ',
    ')
    WITHIN GROUP (ORDER BY TABLE_NAME)||'
)
DISTRIBUTE ON RANDOM;' AS DDL
FROM
    column_def
GROUP BY
    TABLE_NAME
