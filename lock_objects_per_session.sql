SELECT
  object_name
  , object_type
  , session_id
  , type AS lock_type
  , lmode AS lock_mode
  , request
  , block
  , ctime AS time_since_curr_mode
FROM v$locked_object a
JOIN all_objects b
  ON a.object_id = b.object_id
JOIN v$lock c
  ON c.id1 = b.object_id
  AND c.sid = a.session_id
ORDER BY
  session_id
  , ctime DESC
  , object_name
;
