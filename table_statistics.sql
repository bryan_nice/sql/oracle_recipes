SELECT
  owner AS schema
  , table_name
  , last_analyzed
  , stale_stats
FROM all_tab_statistics
;
