SELECT
  cur_session.inst_id
  , cur_session.sid
  , cur_session.username
  , cur_session.sql_exec_start
  , sqltext.piece AS current_sql_piece
  , sqltext.sql_text AS current_sql
FROM gv$sqltext sqltext
LEFT JOIN gv$session cur_session
  ON cur_session.sql_hash_value = sqltext.hash_value
  AND cur_session.sql_address = sqltext.address
WHERE cur_session.username = USER
ORDER BY
  sqltext.piece
;