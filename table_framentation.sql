SELECT
  owner AS schema_name
  , table_name
  , NVL(avg_row_len, 0) AS avg_row_len
  , round(((blocks*16/1024)),2)||'MB' AS fragmented_size
  , round((num_rows*avg_row_len/1024/1024),2)||'MB' AS actual_size
  , round(((blocks*16/1024)-(num_rows*avg_row_len/1024/1024)),2) ||'MB' AS fragmented_space
  , CASE
      WHEN NVL(round(((blocks*16/1024)),2),0) = 0
      THEN 0
      ELSE (round(((blocks*16/1024)-(num_rows*avg_row_len/1024/1024)),2)/round(((blocks*16/1024)),2))*100
  END AS space_fragmentation_percentage
FROM dba_tables
;