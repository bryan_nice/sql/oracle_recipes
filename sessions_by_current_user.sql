SELECT
  inst_id
  , sql_exec_start
  , sql_id
  , prev_sql_id
  , status
FROM gv$session a
WHERE username = USER
ORDER BY
  sql_exec_start DESC
;
