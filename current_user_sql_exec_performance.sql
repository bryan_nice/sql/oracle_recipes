SELECT
  ses.inst_id
  , ses.sid AS session_id
  , ses.username
  , ses.status AS session_status
  , ses.wait_class AS sql_exec_status
  , sq.sql_fulltext
  , sq.sharable_mem
  , sq.persistent_mem
  , sq.runtime_mem
  , sq.sorts
  , sq.fetches
  , sq.executions
  , sq.px_servers_executions
  , sq.end_of_fetch_count
  , sq.loads
  , sq.first_load_time
  , sq.parse_calls
  , sq.disk_reads
  , sq.direct_writes
  , sq.direct_reads
  , sq.buffer_gets
  , sq.application_wait_time
  , sq.concurrency_wait_time
  , sq.cluster_wait_time
  , sq.user_io_wait_time
  , sq.plsql_exec_time
  , sq.rows_processed
  , sq.optimizer_cost
  , sq.cpu_time
  , sq.elapsed_time
  , sq.typecheck_mem
  , sq.io_cell_offload_eligible_bytes
  , sq.io_interconnect_bytes
  , sq.physical_read_requests
  , sq.physical_read_bytes
  , sq.physical_write_requests
  , sq.physical_write_bytes
  , sq.optimized_phy_read_requests
  , sq.locked_total
  , sq.io_cell_uncompressed_bytes
  , sq.io_cell_offload_returned_bytes
FROM gv$session ses
JOIN gv$sql sq
  ON ses.sql_id = sq.sql_id
WHERE ses.username = USER
;

